# Maintainer: tioguda <tioguda@gmail.com>

_pkgname=safenetauthenticationclient
pkgname=safenetauthenticationclient-legacy
_pkgver=10.0.37-0
pkgver=${_pkgver/-/.}
pkgrel=1.2
pkgdesc="SafeNet Authentication Client (version 10.0 for old Alladin eToken support)"
arch=('x86_64')
url="https://cpl.thalesgroup.com/access-management/security-applications/authentication-client-token-management"
license=('custom')
provides=("${_pkgname}=${pkgver}")
conflicts=('sac-core' 'sac-core-legacy' "${_pkgname}")
backup=('etc/eToken.common.conf' 'etc/eToken.conf')
options=('!strip' '!emptydirs')
install=${_pkgname}.install
source=("https://metainfo.manjariando.com.br/${pkgname}/com.SACMonitor.metainfo.xml")
source_x86_64=("https://manjariando.gitlab.io/source-packages/${_pkgname}/SafenetAuthenticationClient-BR-${_pkgver}_amd64.deb")
sha512sums=('cdf65f44889b6ab93f1f6da2b3f1b59f7553a127c8e36befb05fe38556d47050416cacf083d9f389fb09e13770a7a8157a54be5ee7fac1ddc351c0b05647dbf2')
sha512sums_x86_64=('85c583be5f23a48d09a3eb2c094041fa2ce8531a9cdb72828f178ddac7b9be618888b1a9fbc4b7516063aac6891ca526c848eadbb4cf88abcee3710b4cf6ff93')

package(){
    depends=('atk' 'cairo' 'fontconfig' 'freetype2' 'gdk-pixbuf2' 'glib2' 'gtk2' 'kmod' 'libx11' 'openssl-1.0' 'pango' 'pcsc-tools' 'pcsclite' 'openct')

    # Extract package data
    tar xf data.tar.xz -C "${pkgdir}"

    # Fix directories structure differencies
    mkdir -p "${pkgdir}"/usr/lib
    mv "${pkgdir}"/lib/* "${pkgdir}"/usr/lib
    rm -rf "${pkgdir}"/lib "${pkgdir}"/usr/lib64
    rm -rf "${pkgdir}"/etc/{init.d,rc.d}

    # Appstream
    install -Dm644 "${pkgdir}/usr/share/eToken/shortcuts/SACMonitor.desktop" "${pkgdir}/usr/share/applications/com.SACMonitor.desktop"
    install -Dm644 "${pkgdir}/usr/share/eToken/shortcuts/SACTools.desktop" "${pkgdir}/usr/share/applications/com.SACTools.desktop"
    install -Dm644 "${srcdir}/com.SACMonitor.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.SACMonitor.metainfo.xml"
    install -Dm644 "${pkgdir}/usr/share/doc/eToken/LICENSE.txt" "${pkgdir}/usr/share/licenses/${_pkgname}/LICENSE"

    sed -i 's|Comment=SafeNet Authentication Client|Comment=SafeNet Authentication Client Legacy|' "${pkgdir}/usr/share/applications/com.SACMonitor.desktop"
    sed -i 's|Comment=SafeNet Authentication Client Tools|Comment=SafeNet Authentication Client Tools Legacy|' "${pkgdir}/usr/share/applications/com.SACTools.desktop"
    sed -i 's|Terminal=false|Terminal=false\nX-AppStream-Ignore=true|' "${pkgdir}/usr/share/applications/com.SACTools.desktop"

    for i in 16 48 64 72 128 256; do
        install -Dm644 "${pkgdir}/usr/share/eToken/shortcuts/icons/Applc-RTE-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/SACMonitor.png"
        install -Dm644 "${pkgdir}/usr/share/eToken/shortcuts/icons/Applc-RTE-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/SACTools.png"
    done

    install -Dm644 -t "${pkgdir}/etc/xdg/menus/applications-merged" "${pkgdir}/usr/share/eToken/shortcuts/SafeNet.menu"
    install -d "${pkgdir}/usr/share/desktop-directories"
    cp -r "${pkgdir}"/usr/share/eToken/shortcuts/*.directory "${pkgdir}/usr/share/desktop-directories"

    # Archify folder permissions
    cd ${pkgdir}
    for d in $(find . -type d); do
        chmod 755 $d
    done

}
